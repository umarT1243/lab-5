import sys, pygame
import random

pygame.init()

screen = pygame.display.set_mode((500,500))
clock = pygame.time.Clock()

# Initialize variables
show_colour = -1
step = 0
mode = 0
next_mode = 0

def make_random_pattern():
    seq = []
    for i in range(4):
        seq = seq + [random.randint(0,3)]
    return seq

pattern = make_random_pattern()

while True:
    events = pygame.event.get()
    for event in events:
        if event.type == pygame.QUIT:
            sys.exit()

    # Handle Keydown events
    for event in events:
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_q:
                show_colour = 0
                
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_w:
                show_colour = 1   

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_a:
                show_colour = 2 


        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_s:
                show_colour = 3 
                

    if mode == 0:
        if show_colour == -1:
            show_colour = pattern[step]
            step += 1
            if step >= len(pattern):
                next_mode = 1
                step = 0
        else:
            show_colour = -1


     
        
    screen.fill((0,0,0))
    inactive_colour = (150,150,150)
    active_colour = [(255,0,0), (0,255,0), (0,0,255), (255,255,0)][show_colour]
    for i in range(4):
        if i == show_colour:
            pygame.draw.rect(screen, active_colour, (i%2 * 250, i//2 * 250, 250, 250))
        else:
            pygame.draw.rect(screen, inactive_colour, (i%2 * 250, i//2 * 250, 250, 250))



            
    
    clock.tick(3)
    pygame.display.update()
    
    mode = next_mode
